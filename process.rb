module Process
  class << self
    def search(pattern)
      result = Dir['/proc/[0-9]*/cmdline'].inject({}) do |h, file|
        if (process = File.read(file).split(/\000|\s+/).first)
          process = File.basename(process).gsub(/\W/, '')
          (h[process] ||= []).push(file.match(/\d+/)[0].to_i)
        end
        h
      end.map { |k, v| v if k.match(pattern) }.compact.flatten
      result if result.any?
    end
  end
end